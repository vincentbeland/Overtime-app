FactoryGirl.define do

  sequence :email do |n|
     n += 1
    "test#{n}@example.com"
  end

  factory :user do
    first_name "Vincent"
    last_name "Béland"
    email { generate :email }
    password "asdfasdf"
    password_confirmation "asdfasdf"
  end

  factory :admin_user, class: "AdminUser" do
    first_name "Brigitte"
    last_name "Belly"
    email { generate :email }
    password "asdfasdf"
    password_confirmation "asdfasdf"
  end
end